# SuitedConnector Laravel Models

This repository serves as a Composer package containing commonly-used SuitedConnector models for Laravel.

## Including in an existing project

These models are included by default in any project using the [sc-laravel](https://bitbucket.org/suitedconnector/sc-laravel) framework, so if you're using that, no additional steps are necessary.

To include in an existing project:

Add the following in your `composer.json` file

```
    "repositories": [
        {
            "type":"vcs",
            "url": "https://bitbucket.org/suitedconnector/laravel-models.git"
        }
    ],
    "require": {
        "suitedconnector/laravel-models": "dev-master"
    },
```

The models are added to the `App\Models` namespace, and will be usable with:

```
use App\Models\Signup;
```

or:

```
App\Models\Signup::where()
```

etc.