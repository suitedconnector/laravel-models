<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CallPixel extends Model {
    protected $table = 'call_pixels';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = TRUE;

    /* deny mass assignment to these */
    protected $guarded = array('id', 'created_at', 'updated_at');

	/* criteria is a json string -- automatically deserialize it */
	protected $casts = [
		'followup' => 'array'
	];
}
