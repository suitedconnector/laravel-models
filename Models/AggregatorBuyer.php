<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AggregatorBuyer extends Model {
    protected $table = 'aggregator_buyers';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = TRUE;

    /* deny mass assignment to these */
    protected $guarded = ['created_at', 'updated_at'];

    public function caps() {
        return $this->hasMany(AggregatorBuyerCap::class,'buyer_id');
    }
}
