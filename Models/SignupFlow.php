<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SignupFlow extends Model {
	protected $table = 'signup_flow';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = TRUE;

	/* deny mass assignment to these */
	protected $guarded = array('id', 'created_at', 'updated_at');
}
