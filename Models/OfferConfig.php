<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Log;

class OfferConfig extends Model {
	use SoftDeletes;

	protected $guarded = ['id', 'created_at', 'updated_at', 'deleted_at'];

	// Cast payable_criteria JSON to array
	protected $casts = ['payable_criteria' => 'array'];

	public function generateUuid() {
		if (!isset($this->attributes['uuid'])) {
			$this->attributes['uuid'] = (string)Uuid::generate(4);
		}
	}

	public function verticals() {
		return $this->hasManyThrough(Vertical::class, OfferConfigVertical::class);
	}

	public function offerConfigVerticals() {
		return $this->hasMany(OfferConfigVertical::class);
	}

	public function primaryOfferConfigVertical() {
		return $this->belongsTo(OfferConfigVertical::class, 'primary_offer_config_vertical_id');
	}
}
