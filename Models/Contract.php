<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /* many to one relationship to offer */
    public function offer() {
        return $this->belongsTo(Offer::class);
    }
}
