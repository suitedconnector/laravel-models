<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SignupChange extends Model
{
	protected $guarded = ['id'];

	public function signup() {
		return $this->belongsTo('App\Models\Signup');
	}
}
