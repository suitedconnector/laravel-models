<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ThrottleTarget extends Model
{
	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = true;
	public $guarded = ['id', 'updated_at', 'created_at'];

	public function affiliate() {
		return $this->belongsTo('App\Models\Affiliate');
	}

	public function subaffiliate() {
		return $this->belongsTo('App\Models\Subaffiliate', 'name', 'subaffiliate_name');
	}

	public function offer() {
		return $this->belongsTo('App\Models\Offer');
	}
}
