<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExternalVendor extends Model {
	protected $table = 'external_vendors';

	/**
	 * Indicates if the model should be timestamped.
	 *
	 * @var bool
	 */
	public $timestamps = FALSE;

	/* deny mass assignment to these */
	protected $guarded = [
		'id'
	];
}
