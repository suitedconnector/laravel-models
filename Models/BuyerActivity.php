<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BuyerActivity extends Model
{
    protected $table = 'buyeractivity';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = FALSE;

    /* deny mass assignment to these */
    protected $guarded = ['id', 'transaction_date', 'date_updated'];

    protected $dates = [
        'transaction_date',
        'date_updated'
    ];
}
